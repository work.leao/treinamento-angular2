import { Component } from "@angular/core";

@Component({
    selector: 'meu-primeiro-component',
    template: `
        <p> Meu primeiro component com Angular2!
    `
})
export class MeuPrimeiroComponent { }