import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databinding',
  templateUrl: './databinding.component.html',
  /* styleUrls: ['./databinding.component.css'
  ] */
  styles: [`.highlight{
    background-color: yellow;
    font-weight: bold;
}`]
})
export class DatabindingComponent implements OnInit {

  url = 'http://url.com.br';
  cursoAngular = true;
  urlImagem = 'http://lorempixel.com/400/200/nature/';

  valorAtual = '';
  valorSalvo = '';

  isMouseOver: boolean = false;

  getValor() {
    return 1;
  }

  botaoClicado() {
    alert('botão clicado!');
  }

  onKeyUp(evento: KeyboardEvent) {
    /* console.log(evento);
    console.log((<HTMLInputElement>evento.target).value); */
    this.valorAtual = (<HTMLInputElement>evento.target).value;
  }

  salvarValor(valor) {
    this.valorSalvo = valor;
  }

  getCurtirCurso() {
    return true;
  }

  onMouseOverOut() {
    this.isMouseOver = !this.isMouseOver;
  }

  constructor() { }

  ngOnInit() {
  }

}
